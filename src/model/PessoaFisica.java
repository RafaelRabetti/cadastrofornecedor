
package model;

public class PessoaFisica extends Fornecedor{
    
    private String cpf;
    
    public PessoaFisica() 
    {
        
    }
    
    public PessoaFisica(String nome, String telefone) {
        super(nome, telefone);
        this.cpf = cpf;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }
}
