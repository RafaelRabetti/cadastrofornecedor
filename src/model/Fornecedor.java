
package model;

import java.util.ArrayList;


public class Fornecedor {
    protected String nome;
    private String telefone;
    private Endereco endereco;
    private ArrayList<Produto> produtos;
    
    
    public Fornecedor(){
    }
    
    public Fornecedor (String nome, String telefone){
        this.nome = nome;
        this.telefone = telefone;
    }

    public Fornecedor (String nome){
        this.nome = nome;
    }
    
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
    
}
