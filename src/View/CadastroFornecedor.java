
package View;

import controller.ControleFornecedor;
import controller.ControleProduto;
import java.awt.Menu;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import model.Endereco;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;
import model.Produto;

public class CadastroFornecedor extends javax.swing.JFrame {
    
    ControleFornecedor controleFornecedor = new ControleFornecedor();
    private Fornecedor umFornecedor;
    private Produto umProduto;
    ControleProduto controleProduto = new ControleProduto();
    PessoaFisica pessoaFisica = new PessoaFisica();
    PessoaJuridica pessoaJuridica = new PessoaJuridica();
    Produto produto = new Produto();
    ArrayList<Produto> produtos = new ArrayList();
    Endereco endereco = new Endereco();
    ArrayList telefones = new ArrayList();
    private boolean tipoFornecedor;
    String tipoFisico = "Pessoa Física";
    String tipoJuridico = "Pessoa Juridica";
    
    
    public CadastroFornecedor() {
        initComponents();
        this.jTableListaFornecedores.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    CadastroFornecedor(Menu aThis, boolean b) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void exibirInformacao(String info) {
        JOptionPane.showMessageDialog(this, info, "Atenção", JOptionPane.INFORMATION_MESSAGE);
    }
    
    public void limparCampos() {
        jTextFieldNome.setText("");
        jTextFieldTelefone.setText("");
        jTextFieldCnpj.setText("");
        jTextFieldCpf.setText("");
        jTextFieldRazaoSocial.setText("");
        jTextFieldBairro.setText("");
        jTextFieldCidade.setText("");
        jTextFieldComplemento.setText("");
        jTextFieldLogradouro.setText("");
        jTextFieldNumero.setText("");
        jTextFieldPais.setText("");
        jTextFieldEstado.setText("");
        jTextFieldNomeProduto.setText("");
        jTextFieldQuantidade.setText("");
        jTextFieldValorCompra.setText("");
        jTextFieldValorVenda.setText("");
        jTextFieldDescricao.setText("");
    }
    
    private void preencherCampos() {
        jTextFieldNome.setText(umFornecedor.getNome());
    }
    
    private void carregarListaFornecedores(){  
        ArrayList<Fornecedor> listaFornecedores = controleFornecedor.getListaFornecedores();
        DefaultTableModel model = (DefaultTableModel) jTableListaFornecedores.getModel();
        model.setRowCount(0);
        for (Fornecedor f : listaFornecedores) {
            if(tipoFornecedor) 
                model.addRow(new String[]{f.getNome() , f.getTelefone()});
            else if(tipoFornecedor == false)
                 model.addRow(new String[]{f.getNome() , f.getTelefone()}); 
        }
       jTableListaFornecedores.setModel(model);
    }
    
    private void carregarListaProdutos(){
       
        ArrayList<Produto> listaProdutos = controleProduto.getListaProdutos();
        DefaultTableModel model = (DefaultTableModel)jTableListaProdutos.getModel();
        model.setRowCount(0);
        for (Produto p : listaProdutos) {
            String valorCompra = String.valueOf(p.getValorCompra());
            String valorVenda = String.valueOf(p.getValorVenda());
            String quantidade = String.valueOf(p.getQuantidadeEstoque());
            model.addRow(new String[]{p.getNome(), quantidade , valorCompra, valorVenda, });
        }
        jTableListaProdutos.setModel(model);
    
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jLabelNome = new javax.swing.JLabel();
        jCheckBoxPessoaFisica = new javax.swing.JCheckBox();
        jCheckBoxPessoaJuridica = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldCpf = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldCnpj = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldTelefone = new javax.swing.JTextField();
        jButtonSalvar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jTextFieldRazaoSocial = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldLogradouro = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldNumero = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jTextFieldComplemento = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldBairro = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldCidade = new javax.swing.JTextField();
        jTextFieldEstado = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextFieldPais = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        jTextFieldNomeProduto = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jTextFieldValorCompra = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        jTextFieldValorVenda = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jTextFieldQuantidade = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        jTextFieldDescricao = new javax.swing.JTextField();
        jButtonPesquisarProduto = new javax.swing.JButton();
        jButtonPesquisar = new javax.swing.JButton();
        jTextFieldNome = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableListaFornecedores = new javax.swing.JTable();
        jButtonRemoverF = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTableListaProdutos = new javax.swing.JTable();
        jButtonRemoverP = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabelNome.setText("Nome:");

        jCheckBoxPessoaFisica.setText("Pessoa Fisica");
        jCheckBoxPessoaFisica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPessoaFisicaActionPerformed(evt);
            }
        });

        jCheckBoxPessoaJuridica.setText("Pessoa Jurídica");
        jCheckBoxPessoaJuridica.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBoxPessoaJuridicaActionPerformed(evt);
            }
        });

        jLabel3.setText("CPF:");

        jTextFieldCpf.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldCpfKeyTyped(evt);
            }
        });

        jLabel5.setText("CNPJ:");

        jTextFieldCnpj.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldCnpjKeyTyped(evt);
            }
        });

        jLabel2.setText("Telefone:");

        jTextFieldTelefone.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldTelefoneActionPerformed(evt);
            }
        });
        jTextFieldTelefone.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldTelefoneKeyTyped(evt);
            }
        });

        jButtonSalvar.setText("SALVAR");
        jButtonSalvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarActionPerformed(evt);
            }
        });

        jLabel4.setText("Razão Social :");

        jLabel11.setText("Logradouro:");

        jLabel12.setText("Número:");

        jLabel13.setText("Complemento:");

        jLabel10.setText("Bairro:");

        jLabel9.setText("Cidade:");

        jLabel8.setText("Estado:");

        jLabel7.setText("País:");

        jLabel15.setText("Nome do Produto:");

        jLabel16.setText("Valor De Compra:");

        jTextFieldValorCompra.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldValorCompraKeyTyped(evt);
            }
        });

        jLabel17.setText("Valor De Venda:");

        jTextFieldValorVenda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldValorVendaKeyTyped(evt);
            }
        });

        jLabel18.setText("Quantidade:");

        jTextFieldQuantidade.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldQuantidadeActionPerformed(evt);
            }
        });
        jTextFieldQuantidade.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextFieldQuantidadeKeyTyped(evt);
            }
        });

        jLabel19.setText("Descrição:");

        jTextFieldDescricao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldDescricaoActionPerformed(evt);
            }
        });

        jButtonPesquisarProduto.setText("Pesquisar Produto");
        jButtonPesquisarProduto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarProdutoActionPerformed(evt);
            }
        });

        jButtonPesquisar.setText("Pesquisar Fornecedor");
        jButtonPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonPesquisarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelNome)
                            .addComponent(jLabel3)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12)
                            .addComponent(jLabel9)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                            .addComponent(jLabel13)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                                        .addGroup(jPanel2Layout.createSequentialGroup()
                                            .addComponent(jLabel10)
                                            .addGap(70, 70, 70)))
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addComponent(jLabel8)
                                        .addGap(64, 64, 64)))
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, 176, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jCheckBoxPessoaFisica)
                                                    .addComponent(jCheckBoxPessoaJuridica)))
                                            .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, 378, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, 380, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(52, 52, 52)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jLabel16)
                                                    .addComponent(jLabel17)
                                                    .addComponent(jLabel18)
                                                    .addComponent(jLabel19))
                                                .addGap(30, 30, 30)
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(jTextFieldValorVenda, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGap(0, 0, Short.MAX_VALUE))
                                                    .addComponent(jTextFieldDescricao)))
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jButtonPesquisarProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(jPanel2Layout.createSequentialGroup()
                                                        .addComponent(jLabel15)
                                                        .addGap(24, 24, 24)
                                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                            .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                            .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                                .addGap(0, 19, Short.MAX_VALUE)))))))
                        .addContainerGap())
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel2)
                            .addComponent(jLabel4)
                            .addComponent(jLabel7)
                            .addComponent(jButtonPesquisar))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(497, 497, 497))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(0, 3, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNome)
                    .addComponent(jTextFieldNomeProduto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15)
                    .addComponent(jTextFieldNome, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldCpf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jCheckBoxPessoaFisica)
                    .addComponent(jLabel16)
                    .addComponent(jTextFieldValorCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel5)
                                .addComponent(jTextFieldCnpj, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel17)
                                .addComponent(jTextFieldValorVenda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(jTextFieldRazaoSocial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jTextFieldQuantidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(jTextFieldTelefone, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel19)
                            .addComponent(jTextFieldDescricao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel11)
                            .addComponent(jTextFieldLogradouro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel12)
                                    .addComponent(jTextFieldNumero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel13)
                                    .addComponent(jTextFieldComplemento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(30, 30, 30)
                                .addComponent(jButtonPesquisarProduto)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel10)
                            .addComponent(jTextFieldBairro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(jTextFieldCidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel8)
                            .addComponent(jTextFieldEstado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7)
                            .addComponent(jTextFieldPais, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jButtonPesquisar))
                    .addComponent(jCheckBoxPessoaJuridica))
                .addGap(17, 17, 17)
                .addComponent(jButtonSalvar, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(54, 54, 54))
        );

        jTabbedPane1.addTab("Cadastro", jPanel2);

        jTableListaFornecedores.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Telefones"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableListaFornecedores.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableListaFornecedoresMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableListaFornecedores);

        jButtonRemoverF.setText("Remover");
        jButtonRemoverF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoverFActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 667, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonRemoverF))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(62, 62, 62)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addComponent(jButtonRemoverF)
                .addContainerGap(214, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Fornecedores", jPanel3);

        jTableListaProdutos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Nome", "Quantidade", "Valor De Compra", "Valor De Venda"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTableListaProdutos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableListaProdutosMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jTableListaProdutos);

        jButtonRemoverP.setText("Remover");
        jButtonRemoverP.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRemoverPActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 667, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonRemoverP, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(524, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(jButtonRemoverP)
                .addContainerGap(82, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Produtos", jPanel4);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1199, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 620, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jCheckBoxPessoaFisicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPessoaFisicaActionPerformed
        jCheckBoxPessoaJuridica.setSelected(false);
        jTextFieldRazaoSocial.setEnabled(false);
        jTextFieldCnpj.setEnabled(false);
        jTextFieldRazaoSocial.setText("");
        jTextFieldCnpj.setText("");
        jTextFieldCpf.setEnabled(true);
        tipoFornecedor = true;
    }//GEN-LAST:event_jCheckBoxPessoaFisicaActionPerformed

    private void jCheckBoxPessoaJuridicaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBoxPessoaJuridicaActionPerformed
        jCheckBoxPessoaFisica.setSelected(false);
        jTextFieldCpf.setEnabled(false);
        jTextFieldCpf.setText("");
        jTextFieldRazaoSocial.setEnabled(true);
        jTextFieldCnpj.setEnabled(true);
        tipoFornecedor = false;
    }//GEN-LAST:event_jCheckBoxPessoaJuridicaActionPerformed

    private void jTextFieldCpfKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldCpfKeyTyped
        
    }//GEN-LAST:event_jTextFieldCpfKeyTyped

    private void jTextFieldCnpjKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldCnpjKeyTyped
        
    }//GEN-LAST:event_jTextFieldCnpjKeyTyped

    private void jTextFieldTelefoneKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldTelefoneKeyTyped
       
    }//GEN-LAST:event_jTextFieldTelefoneKeyTyped

    private void jButtonSalvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarActionPerformed

        if(jTextFieldCpf.isEnabled())
        {

            String nome = jTextFieldNome.getText();
            pessoaFisica.setNome(nome);

            String cpf = jTextFieldCpf.getText();
            pessoaFisica.setCpf(cpf);

            String telefone = jTextFieldTelefone.getText();
            pessoaFisica.setTelefone(telefone);

            endereco.setBairro(jTextFieldBairro.getText());
            endereco.setCidade(jTextFieldCidade.getText());
            endereco.setComplemento(jTextFieldComplemento.getText());
            endereco.setLogradouro(jTextFieldLogradouro.getText());
            endereco.setNumero(jTextFieldNumero.getText());
            endereco.setPais(jTextFieldPais.getText());
            endereco.setEstado(jTextFieldEstado.getText());

            pessoaFisica.setEndereco(endereco);

            produto.setNome(jTextFieldNomeProduto.getText());
            produto.setDescricao(jTextFieldDescricao.getText());

            pessoaFisica.setProdutos(produtos);
            carregarListaProdutos();

            if(jTextFieldNome.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um nome para o fornecedor.");
            else if(jTextFieldTelefone.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um telefone para o fornecedor.");
            else if(jTextFieldCpf.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um CPF para o fornecedor.");
            else if(jTextFieldNomeProduto.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um nome para o produto.");
            else if(jTextFieldValorCompra.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um valor de compra para o produto.");
            else if(jTextFieldValorVenda.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um valor de venda para o produto.");
            else if(jTextFieldQuantidade.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira a quantidade disponivel do produto no estoque.");
            else {
                double valorCompra = Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompra);

                double valorVenda = Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVenda);

                double quantidadeEstoque = Double.parseDouble(jTextFieldQuantidade.getText());
                produto.setQuantidadeEstoque(quantidadeEstoque);

                controleFornecedor.adicionar(pessoaFisica);
                controleProduto.adicionarProduto(produto);
                JOptionPane.showMessageDialog(rootPane, jTextFieldNome.getText() + " adicionado(a) com sucesso", "Sucesso", WIDTH, null);
                pessoaFisica = new PessoaFisica();

                limparCampos();
                produto = new Produto();
            }

        }

        else if(jTextFieldCnpj.isEnabled())
        {
            String nome = jTextFieldNome.getText();
            pessoaJuridica.setNome(nome);

            String telefone = jTextFieldTelefone.getText();
            pessoaJuridica.setTelefone(telefone);

            String razaoSocial = jTextFieldRazaoSocial.getText();
            pessoaJuridica.setRazaoSocial(razaoSocial);

            String cnpj = jTextFieldCnpj.getText();
            pessoaJuridica.setCnpj(cnpj);

            endereco.setBairro(jTextFieldBairro.getText());
            endereco.setCidade(jTextFieldCidade.getText());
            endereco.setComplemento(jTextFieldComplemento.getText());
            endereco.setLogradouro(jTextFieldLogradouro.getText());
            endereco.setNumero(jTextFieldNumero.getText());
            endereco.setPais(jTextFieldPais.getText());
            endereco.setEstado(jTextFieldEstado.getText());

            pessoaJuridica.setEndereco(endereco);

            produto.setNome(jTextFieldNomeProduto.getText());
            produto.setDescricao(jTextFieldDescricao.getText());

            pessoaJuridica.setProdutos(produtos);
            carregarListaProdutos();

            if(jTextFieldNome.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um nome para o fornecedor.");
            else if(jTextFieldRazaoSocial.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira uma Razão Social para o fornecedor.");
            else if(jTextFieldTelefone.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um telefone para o fornecedor.");
            else if(jTextFieldCnpj.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um CNPJ para o fornecedor.");
            else if(jTextFieldNomeProduto.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um nome para o produto.");
            else if(jTextFieldValorCompra.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um valor de compra para o produto.");
            else if(jTextFieldValorVenda.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira um valor de venda para o produto.");
            else if(jTextFieldQuantidade.getText().equals(""))
            JOptionPane.showMessageDialog(rootPane, "Insira a quantidade disponivel do produto no estoque.");
            else {
                double valorCompra = Double.parseDouble(jTextFieldValorCompra.getText());
                produto.setValorCompra(valorCompra);

                double valorVenda = Double.parseDouble(jTextFieldValorVenda.getText());
                produto.setValorVenda(valorVenda);

                double quantidadeEstoque = Double.parseDouble(jTextFieldQuantidade.getText());
                produto.setQuantidadeEstoque(quantidadeEstoque);
                controleFornecedor.adicionar(pessoaJuridica);
                controleProduto.adicionarProduto(produto);
                JOptionPane.showMessageDialog(rootPane, jTextFieldNome.getText() + " adicionado(a) com sucesso", "Sucesso", WIDTH, null);
                limparCampos();
                pessoaJuridica = new PessoaJuridica();
                produto = new Produto();
            }
        }
        
        limparCampos();
        carregarListaFornecedores();
        carregarListaProdutos();
    }//GEN-LAST:event_jButtonSalvarActionPerformed

    private void jTextFieldValorCompraKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldValorCompraKeyTyped
     
    }//GEN-LAST:event_jTextFieldValorCompraKeyTyped

    private void jTextFieldValorVendaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldValorVendaKeyTyped
        
    }//GEN-LAST:event_jTextFieldValorVendaKeyTyped

    private void jTextFieldQuantidadeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldQuantidadeActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldQuantidadeActionPerformed

    private void jTextFieldQuantidadeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextFieldQuantidadeKeyTyped
        
    }//GEN-LAST:event_jTextFieldQuantidadeKeyTyped

    private void jButtonPesquisarProdutoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarProdutoActionPerformed
        String nomeProdutoPesquisa = JOptionPane.showInputDialog("Insira o nome do Produto:");

        Produto produtoPesquisa = controleProduto.pesquisar(nomeProdutoPesquisa);

        if(produtoPesquisa != null){
            jTextFieldNomeProduto.setText(produtoPesquisa.getNome());
            jTextFieldDescricao.setText(produtoPesquisa.getDescricao());
            String valorCompra = String.valueOf(produtoPesquisa.getValorCompra());
            String valorVenda = String.valueOf(produtoPesquisa.getValorVenda());
            String quantidade = String.valueOf(produtoPesquisa.getQuantidadeEstoque());
            jTextFieldValorVenda.setText(valorVenda);
            jTextFieldValorCompra.setText(valorCompra);
            jTextFieldQuantidade.setText(quantidade);
        }
        else
        JOptionPane.showMessageDialog(rootPane, "Produto não encontrado.");

        carregarListaProdutos();

    }//GEN-LAST:event_jButtonPesquisarProdutoActionPerformed

    private void jButtonPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonPesquisarActionPerformed

        String nomePesquisa = JOptionPane.showInputDialog("Insira o nome do Fornecedor:");

        Fornecedor fornecedorPesquisa = controleFornecedor.pesquisar(nomePesquisa);

        if(fornecedorPesquisa != null){
            if(tipoFornecedor){
                jTextFieldBairro.setText(fornecedorPesquisa.getEndereco().getBairro());
                jTextFieldCidade.setText(fornecedorPesquisa.getEndereco().getCidade());
                jTextFieldCnpj.setText(null);
                jTextFieldRazaoSocial.setText(null);
                jTextFieldComplemento.setText(fornecedorPesquisa.getEndereco().getComplemento());

                PessoaFisica fornecedorFisicoPesquisa = controleFornecedor.pesquisarPessoaFisica(nomePesquisa);
                jTextFieldCpf.setText(fornecedorFisicoPesquisa.getCpf());

                jTextFieldLogradouro.setText(fornecedorPesquisa.getEndereco().getLogradouro());
                jTextFieldNome.setText(fornecedorPesquisa.getNome());
                jTextFieldNumero.setText(fornecedorPesquisa.getEndereco().getNumero());
                jTextFieldPais.setText(fornecedorPesquisa.getEndereco().getPais());
                jTextFieldTelefone.setText(fornecedorPesquisa.getTelefone());
                jTextFieldEstado.setText(fornecedorPesquisa.getEndereco().getEstado());
            }

            else if(tipoFornecedor == false){
                jTextFieldNome.setText(fornecedorPesquisa.getNome());

                PessoaJuridica fornecedorJuridicoPesquisa = controleFornecedor.pesquisarPessoaJuridica(nomePesquisa);
                jTextFieldRazaoSocial.setText(fornecedorJuridicoPesquisa.getRazaoSocial());

                jTextFieldBairro.setText(fornecedorPesquisa.getEndereco().getBairro());
                jTextFieldCidade.setText(fornecedorPesquisa.getEndereco().getCidade());
                jTextFieldCpf.setText(null);
                jTextFieldComplemento.setText(fornecedorPesquisa.getEndereco().getComplemento());

                PessoaJuridica fornecedorJuridicoPesquisa2 = controleFornecedor.pesquisarPessoaJuridica(nomePesquisa);
                jTextFieldCnpj.setText(fornecedorJuridicoPesquisa2.getCnpj());

                jTextFieldLogradouro.setText(fornecedorPesquisa.getEndereco().getLogradouro());
                jTextFieldEstado.setText(fornecedorPesquisa.getEndereco().getEstado());
                jTextFieldNumero.setText(fornecedorPesquisa.getEndereco().getNumero());
                jTextFieldPais.setText(fornecedorPesquisa.getEndereco().getPais());
                jTextFieldTelefone.setText(fornecedorPesquisa.getTelefone());
            }
        }
        else
        JOptionPane.showMessageDialog(rootPane, "Fornecedor não encontrado.");
    }//GEN-LAST:event_jButtonPesquisarActionPerformed

    private void jTableListaFornecedoresMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaFornecedoresMouseClicked
    if (jTableListaFornecedores.isEnabled()) {       
            DefaultTableModel model = (DefaultTableModel) jTableListaFornecedores.getModel();
            String umNome = (String) model.getValueAt(jTableListaFornecedores.getSelectedRow(), 0);
            this.pesquisarFornecedor(umNome);
        }
    }//GEN-LAST:event_jTableListaFornecedoresMouseClicked

    private void jTextFieldDescricaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldDescricaoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldDescricaoActionPerformed

    private void jTextFieldTelefoneActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldTelefoneActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldTelefoneActionPerformed

    private void jButtonRemoverFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverFActionPerformed
        this.controleFornecedor.remover(umFornecedor);       
        this.carregarListaFornecedores();
    }//GEN-LAST:event_jButtonRemoverFActionPerformed

    private void jTableListaProdutosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableListaProdutosMouseClicked
        if (jTableListaProdutos.isEnabled()) {       
            DefaultTableModel model = (DefaultTableModel) jTableListaProdutos.getModel();
            String umNome = (String) model.getValueAt(jTableListaProdutos.getSelectedRow(), 0);
            this.pesquisarProduto(umNome);
        }
    }//GEN-LAST:event_jTableListaProdutosMouseClicked

    private void jButtonRemoverPActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRemoverPActionPerformed
        this.controleProduto.removerProduto(umProduto);       
        this.carregarListaProdutos();
    }//GEN-LAST:event_jButtonRemoverPActionPerformed

  
    private void pesquisarFornecedor(String nome) {
        Fornecedor fornecedorPesquisado = controleFornecedor.pesquisar(nome);

        if (fornecedorPesquisado == null) {
            exibirInformacao("Fornecedor não encontrado.");
        } else {
            this.umFornecedor = fornecedorPesquisado;
            this.preencherCampos();
        }
    }
    
    private void pesquisarProduto(String nome) {
        Produto produtoPesquisado = controleProduto.pesquisar(nome);

        if (produtoPesquisado == null) {
            exibirInformacao("Produto não encontrado.");
        } else {
            this.umProduto = produtoPesquisado;
            this.preencherCampos();
        }
    }
    
    
    public static void main(String args[]) {
        
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CadastroFornecedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CadastroFornecedor().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonPesquisar;
    private javax.swing.JButton jButtonPesquisarProduto;
    private javax.swing.JButton jButtonRemoverF;
    private javax.swing.JButton jButtonRemoverP;
    private javax.swing.JButton jButtonSalvar;
    private javax.swing.JCheckBox jCheckBoxPessoaFisica;
    private javax.swing.JCheckBox jCheckBoxPessoaJuridica;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabelNome;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTable jTableListaFornecedores;
    private javax.swing.JTable jTableListaProdutos;
    private javax.swing.JTextField jTextFieldBairro;
    private javax.swing.JTextField jTextFieldCidade;
    private javax.swing.JTextField jTextFieldCnpj;
    private javax.swing.JTextField jTextFieldComplemento;
    private javax.swing.JTextField jTextFieldCpf;
    private javax.swing.JTextField jTextFieldDescricao;
    private javax.swing.JTextField jTextFieldEstado;
    private javax.swing.JTextField jTextFieldLogradouro;
    private javax.swing.JTextField jTextFieldNome;
    private javax.swing.JTextField jTextFieldNomeProduto;
    private javax.swing.JTextField jTextFieldNumero;
    private javax.swing.JTextField jTextFieldPais;
    private javax.swing.JTextField jTextFieldQuantidade;
    private javax.swing.JTextField jTextFieldRazaoSocial;
    private javax.swing.JTextField jTextFieldTelefone;
    private javax.swing.JTextField jTextFieldValorCompra;
    private javax.swing.JTextField jTextFieldValorVenda;
    // End of variables declaration//GEN-END:variables
}
