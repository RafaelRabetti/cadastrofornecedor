package controller;

import java.util.ArrayList;
import model.Produto;

public class ControleProduto {
    
    private ArrayList<Produto> listaProdutos;
    
    public ControleProduto () {
        listaProdutos = new ArrayList<Produto>();
    }
    
    public ArrayList getListaProdutos(){
        return listaProdutos;
    }
    
    public String adicionarProduto (Produto umProduto) {
        String mensagem = "Produto adicionado com sucesso!";
        listaProdutos.add(umProduto);
        return mensagem;
    }
    
    public String removerProduto (Produto umProduto) {
        String mensagem = "Produto removido com sucesso!";
        listaProdutos.remove(umProduto);
        return mensagem;
    }
    
    public Produto pesquisar (String nomeProduto) {
        for (Produto p : listaProdutos) {
            if (p.getNome().equalsIgnoreCase(nomeProduto)) 
                return p;
        }
        return null;
    }
}
