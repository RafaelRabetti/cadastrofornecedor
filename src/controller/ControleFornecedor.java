package controller;

import java.awt.Menu;
import java.util.ArrayList;
import java.util.Iterator;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.table.DefaultTableModel;
import model.Fornecedor;
import model.PessoaFisica;
import model.PessoaJuridica;

public class ControleFornecedor {
    
    private ArrayList<Fornecedor> listaFornecedores;    
    
    public ControleFornecedor(){
        listaFornecedores = new ArrayList<Fornecedor>();
    }
    
    public ArrayList<Fornecedor> getListaFornecedores(){        
        return listaFornecedores;
    }
    
    public String adicionar (PessoaFisica fornecedor) {        
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Física adicionado com sucesso";
    }
    
    public String adicionar (PessoaJuridica fornecedor) {        
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa Jurídica adicionado com sucesso";
    }
    
    public Fornecedor pesquisar (String nome){
        for(Fornecedor f: listaFornecedores){
            if(f.getNome().equalsIgnoreCase(nome)) return f;       
        }
        return null;
    }
    
    public void remover(Fornecedor umFornecedor){
        listaFornecedores.remove(umFornecedor);
    }
            
            
    public void remover(PessoaFisica fornecedor){
        listaFornecedores.remove(fornecedor);
    }
    
    public void remover(PessoaJuridica fornecedor){
        listaFornecedores.remove(fornecedor);
    }
    
    public PessoaFisica pesquisarPessoaFisica(String nome){
       for (Iterator<Fornecedor> it = listaFornecedores.iterator(); it.hasNext();) {
            PessoaFisica fornecedor = (PessoaFisica) it.next();
            if(fornecedor.getNome().equalsIgnoreCase(nome))
                return fornecedor;
        }
        return null;
    }
    public PessoaJuridica pesquisarPessoaJuridica(String nome){
       for (Iterator<Fornecedor> it = listaFornecedores.iterator(); it.hasNext();) {
            PessoaJuridica fornecedor = (PessoaJuridica) it.next();
            if(fornecedor.getNome().equalsIgnoreCase(nome))
                return fornecedor;
        }
        return null;
    }
}
    

